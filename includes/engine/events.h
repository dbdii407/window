#pragma once

#include "ptyps/utils/events.h"
#include <SDL2/SDL.h>

namespace engine {
  typedef enum {
    QUIT,

    // 100
    WINDOW_OPEN = 0x64, WINDOW_HIDE, WINDOW_RESIZE, WINDOW_SIZE, WINDOW_CLOSE,
    WINDOW_MOVE, WINDOW_EXPOSED, WINDOW_FOCUS, WINDOW_DEFOCUS, WINDOW_MAXIMIZE,
    WINDOW_MINIMIZE,

    // 200
    MOUSE_ENTER = 0xC8, MOUSE_LEAVE, MOUSE_POSITION, MOUSE_PRESS, MOUSE_RELEASE,
    MOUSE_SCROLL,
  } event_t;

  class Listener : public ptyps::utils::EventEmitter {
    private:
      SDL_Event poll() {
        auto event = SDL_Event();
        SDL_PollEvent(&event);
        return event;
      }

    protected:
      using ptyps::utils::EventEmitter::emit;

    public:
      Listener() : ptyps::utils::EventEmitter() {

      }

      void run() {
        while (!0) {
          auto event = poll();

          if (event.type == SDL_QUIT)
            return emit(QUIT);
          
          else if (event.type == SDL_MOUSEMOTION)
            emit(MOUSE_POSITION, event.window.windowID, event.motion.x, event.motion.y);

          else if (event.type == SDL_WINDOWEVENT) {
            switch (event.window.event) {
              case SDL_WINDOWEVENT_SHOWN:
                emit(WINDOW_OPEN, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_HIDDEN:
                emit(WINDOW_HIDE, event.window.windowID);
                break;
       
              case SDL_WINDOWEVENT_RESIZED:
                emit(WINDOW_RESIZE, event.window.windowID, event.window.data1, event.window.data2);
                break;

              case SDL_WINDOWEVENT_SIZE_CHANGED:
                emit(WINDOW_SIZE, event.window.windowID, event.window.data1, event.window.data2);
                break;

              case SDL_WINDOWEVENT_CLOSE:
                emit(WINDOW_CLOSE, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_MOVED:
                emit(WINDOW_MOVE, event.window.windowID, event.window.data1, event.window.data2);
                break;

              case SDL_WINDOWEVENT_EXPOSED:
                emit(WINDOW_EXPOSED, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_FOCUS_GAINED:
                emit(WINDOW_FOCUS, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_FOCUS_LOST:
                emit(WINDOW_DEFOCUS, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_LEAVE:
                emit(MOUSE_LEAVE, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_ENTER:
                emit(MOUSE_ENTER, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_MINIMIZED:
                emit(WINDOW_MINIMIZE, event.window.windowID);
                break;

              case SDL_WINDOWEVENT_MAXIMIZED:
                emit(WINDOW_MAXIMIZE, event.window.windowID);
                break;
            }
          }
        }
      }
  };
}