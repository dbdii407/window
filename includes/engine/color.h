#pragma once

#include <tuple>

namespace engine {
  typedef std::tuple<double, double, double> rgb_t;

  typedef enum {
    RED = 0xff0000,
    ORANGE = 0xffa500,
    YELLOW = 0xffff00,
    GREEN = 0x008000,
    BLUE = 0x0000ff,
    PURPLE = 0x800080,
    INDIGO = 0x4b0082,
    VIOLET = 0xee82ee,
    CYAN = 0x00ffff,
    BLACK = 0x000000,
    GRAY = 0x808080,
    LIGHT_GRAY = 0xd3d3d3,
    DARK_GRAY = 0xa9a9a9,
    WHITE = 0xffffff,
    SALMON = 0xFA8072,
    
    // Orange
    TOMATO = 0xff6347,

    // Red
    CRIMSON = 0xdc143c,
    FIREBRICK = 0xb22222,
    RED_1 = 0x8a2f35,

    // PURPLE
    PURPLE_1 = 0x473a4d

  } hex_t;

  rgb_t to_rgb(hex_t hex) {
    double r = ((hex >> 16) & 0xFF) / 255.0;
    double g = ((hex >> 8) & 0xFF) / 255.0;
    double b = (hex & 0xFF) / 255.0;
    return rgb_t(r, g, b);
  }
}