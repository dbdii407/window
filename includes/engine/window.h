#pragma once

#include "color.h"

#include <SDL2/SDL.h>
#include <string>

namespace engine {
  class Window {
    private:
      SDL_Window* window;

      SDL_Surface* getSurface() {
        return SDL_GetWindowSurface(window);
      }

      SDL_Renderer* getRender() {
        return SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
      }

      void update() {
        SDL_UpdateWindowSurface(window);
      }

    public:
      Window(std::string title) {
        window = SDL_CreateWindow(NULL, 0, 0, 0, 0, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

        set_title(title);
      }

      uint get_id() {
        return SDL_GetWindowID(window);
      }

      // ----

      void set_title(std::string title) {
        SDL_SetWindowTitle(window, &title[0]);
      }

      std::string get_title() {
        return SDL_GetWindowTitle(window);
      }

      // ----

      void set_size(uint width, uint height) {
        SDL_SetWindowSize(window, width, height);
      }

      std::tuple<int, int> get_size() {
        int width, height;
        SDL_GetWindowSize(window, &width, &height);
        return std::make_tuple(height, width);
      }

      // ----

      void set_position(uint x, uint y) {
        SDL_SetWindowPosition(window, x, y);
      }

      // ----

      void fill(hex_t hex) {
        auto surface = getSurface();
        SDL_FillRect(surface, NULL, hex);
        update();
      }

      void clear() {
        auto render = getRender();
        SDL_RenderClear(render);
        update();
      }

      void show() {
        SDL_ShowWindow(window);
      }

      void destroy() {
        SDL_DestroyWindow(window);
        SDL_Quit();
      }
  };
}