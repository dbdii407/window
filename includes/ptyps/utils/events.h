#pragma once

#include <functional>
#include <memory>
#include <list>

namespace ptyps::utils {
  class EventEmitter {
    private:
      struct Base {
        Base &operator=(const Base &) = delete;
        Base(const Base &) = delete;
        explicit Base() = default;
        virtual ~Base() { }
      };

      template <typename ...args> struct Listener : public Base {
        private:
          typedef std::function<void (args ...)> funct_t;
          funct_t fn;

        public:
          explicit Listener(funct_t fn) : fn(fn) { }
          Listener(const Listener &) = delete;
          Listener &operator=(const Listener &) = delete;
          virtual ~Listener() { }
          
          void call(args ...argl) {
            fn(argl ...);
          }
      };

      typedef std::pair<int, std::shared_ptr<Base>> listener_t;

      std::list<listener_t> listeners;

    private:
      template <typename T> struct function_traits : public function_traits<decltype(&T::operator())> {
        
      };

      template <typename C, typename R, typename ...args> struct function_traits<R(C::*)(args ...) const> {
        typedef std::function<R (args ...)> f_type;
      };

      template <typename L> typename function_traits<L>::f_type make_function(L l) {
        return (typename function_traits<L>::f_type)(l);
      }

    public:
      template <typename ...args> void on(int name, std::function<void (args ...)> func) {
        auto listener = listener_t(name, std::make_shared<Listener<args ...>>(func));
        listeners.push_back(listener);
      }

      template <typename F> void on(int name, F func) {
        on(name, make_function(func));
      }

      template <typename ...args> void emit(int name, args ...argl) {
        auto begin = std::begin(listeners);
        auto end = std::end(listeners);

        std::for_each(begin, end, [&](listener_t listener) {
          if (listener.first != name)
            return;

          static_cast<Listener<args ...>*>(listener.second.get())->call(argl ...);
        });
      }
  };
}
