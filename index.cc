#include "engine/window.h"
#include "engine/events.h"
#include "engine/color.h"

// g++ -std=c++17 $(pkg-config -libs sdl2) -Iincludes/ index.cc -o index

int main() {
  auto window = new engine::Window("Example Window");

  auto events = new engine::Listener();

  events->on(engine::WINDOW_OPEN, [&](uint id) {
    if (id != window->get_id())
      return;

    window->fill(engine::BLACK);
    
    printf("window opened\n");
  });

  events->on(engine::WINDOW_CLOSE, [&](uint id) {
    if (id != window->get_id())
      return;

    printf("window closing\n");
  });

  events->on(engine::WINDOW_FOCUS, [&](uint id) {
    if (id != window->get_id())
      return;

    printf("window focused\n");
  });

  events->on(engine::WINDOW_DEFOCUS, [&](uint id) {
    if (id != window->get_id())
      return;

    printf("window defocused\n");
  });

  events->on(engine::WINDOW_MINIMIZE, [&](uint id) {
    if (id != window->get_id())
      return;

    printf("window minimized\n");
  });

  events->on(engine::WINDOW_RESIZE, [&](uint id, int width, int height) {
    if (id != window->get_id())
      return;

    window->fill(engine::BLACK);
    
    printf("window resized %i %i\n", width, height);
  });

  events->on(engine::WINDOW_MOVE, [&](uint id, int x, int y) {
    if (id != window->get_id())
      return;

    printf("window moved: %i %i\n", x, y);
  });

  window->set_position(1020, 220);
  window->set_size(400, 200);
  window->show();

  events->run();
}